define([
  'dojo/_base/declare', 'dojo/on', 'dojo/_base/lang',
  'jimu/BaseWidget'
], function(
  declare, on, lang,
  BaseWidget
  /*CAUTION: do NOT put 'comma' at the last of the object. IE will detect this as an error...*/
) {
  return declare([BaseWidget], {
    baseClass: 'jimu-widget-compassrose',
    name: 'Compass Rose',

    _imgWidth : null,
    _imgHeight: null,
    _imageEl: null,
    _is_shown_ : false,

    //Element to put the image el:
    _parentEl: null,

    /**
     * Boot this widget
     */
    startup: function() {
      this.inherited(arguments);
      // Prepare root element:
      this._parentEl = document.getElementById('main-page');
      this._is_shown_ = false;
      
      // Obtain parameters from config:
      var imageUrl = this.folderUrl + this.config.imagePath;
      this._imgWidth = this.config.imageWidth;
      this._imgHeight = this.config.imageHeight;
      var imageLeftPos = (this.map.width / 2) - (this._imgWidth / 2);
      var imageTopPos = (this.map.height / 2) - (this._imgHeight / 2);
      var imageOpacity = this.config.imageOpacity;
      var imageOpacityIEFallback = 100 * imageOpacity;
      
      // Prepare image element:
      this._imageEl = document.createElement('img');
      this._imageEl.setAttribute('id', 'compassroseimage');
      this._imageEl.src = imageUrl;
      this._imageEl.width = this._imgWidth;
      this._imageEl.height = this._imgHeight;
      this._imageEl.style.display = 'block';
      this._imageEl.style.zIndex = 100;
      this._imageEl.style.position = 'absolute';
      this._imageEl.style.left = imageLeftPos + 'px';
      this._imageEl.style.top = imageTopPos + 'px';
      // Opacity:
      this._imageEl.style.opacity = imageOpacity;
      this._imageEl.style.filter = 'alpha(opacity='+imageOpacityIEFallback+')'; //IE fallback
      // prevent user drag the compass image:
      this._imageEl.setAttribute('draggable', 'false');
      this._imageEl.style.userSelect = 'none';

      //set window=map resize event:
      //NOT THIS: /*on(window, 'resize', lang.hitch(this, this.onWindowResized));*/
      this.map.on('extent-change', lang.hitch(this, this.onWindowResized));
    },

    /**
     * Destroy this widget
     */
    destroy: function() {
      this._imageEl = null;
    },

    /**
     * Display compass
     */
    onOpen: function(){
      this._parentEl.parentNode.insertBefore(this._imageEl, this._parentEl);
      this._is_shown_ = true;
    },

    /**
     * Hide compass
     */
    onClose: function() {
      this._parentEl.parentNode.removeChild(this._imageEl);
      this._is_shown_ = false;
    },

    /**
     * if the map (window) has resized:
     */
    onWindowResized: function() {
      if (this._is_shown_) {
        //remove image:
        this._parentEl.parentNode.removeChild(this._imageEl);
        // reset position:
        var imageLeftPos = (this.map.width / 2) - (this._imgWidth / 2);
        var imageTopPos = (this.map.height / 2) - (this._imgHeight / 2);
        this._imageEl.style.left = imageLeftPos + 'px';
        this._imageEl.style.top = imageTopPos + 'px';
        // re-dislpay it:
        this._parentEl.parentNode.insertBefore(this._imageEl, this._parentEl);
      }
    }
  });
});