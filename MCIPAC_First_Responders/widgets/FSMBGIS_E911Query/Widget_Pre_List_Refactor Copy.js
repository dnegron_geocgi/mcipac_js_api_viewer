define(['dojo/_base/declare',
'jimu/BaseWidget',
'dojo/_base/lang',
'dojo/Deferred',
'dgrid/OnDemandList',
'dgrid/Selection',
"dojo/store/Memory",
"esri/tasks/query",
"esri/tasks/QueryTask",
'esri/graphic',
"esri/symbols/SimpleMarkerSymbol",
"esri/symbols/SimpleLineSymbol",
"esri/symbols/SimpleFillSymbol",
"esri/symbols/PictureMarkerSymbol",
"jimu/LayerStructure",
"esri/layers/FeatureLayer",
"dojo/_base/Color",
'dijit/layout/TabContainer',
'dijit/layout/ContentPane',
'dojo/dom-attr',
'dojo/query',
'dijit/layout/AccordionContainer',
'dojo/request',
"dijit/_WidgetBase",
"dijit/_TemplatedMixin",
'dijit/registry',
"esri/geometry/projection",
"esri/geometry/Point",
'esri/graphicsUtils',
"esri/geometry/geometryEngine",
"esri/geometry/Extent",
"dojox/xml/parser",
"esri/symbols/Font",
"esri/symbols/TextSymbol",
"esri/renderers/SimpleRenderer",
"esri/renderers/ScaleDependentRenderer",
"esri/InfoTemplate", 
"esri/layers/GraphicsLayer",
'dojo/domReady!'],
function(declare, BaseWidget,
  lang, Deferred,
  OnDemandList, Selection, Memory, Query, QueryTask, Graphic, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol, PictureMarkerSymbol, LayerStructure, FeatureLayer, Color, TabContainer, ContentPane, attr, DojoQuery, AccordionContainer, Request,  _WidgetBase, _TemplateMixin, registry, Projection, Point, GraphicsUtils,geometryEngine, Extent, xmlParser, Font, TextSymbol, SimpleRenderer, ScaleDependentRenderer, InfoTemplate, GraphicsLayer) {
  //To create a widget, you need to derive from BaseWidget.
  return declare([BaseWidget], {
    
    defaultSymbol: new PictureMarkerSymbol("widgets/FSMBGIS_E911Query/images/i_pin.png", 51, 51),
      
    onBaseTC: false,
      
    offBaseTC: false,
      
    gunList: [],
      
    cityList: [],
      
    sectionList: [],
      
    symbolColor50: [196,252,255,0.5],
    
    addressLayerUrl: "https://devserver.local/server/rest/services/fr/pmo_for_queries/MapServer/0",
            
    streetLayer: "https://devserver.local/server/rest/services/fr/pmo_for_queries/MapServer/5",
      
    landmarkSearch: false,
      
    ListNodeVar:  null,
      
    spatialFilter: null,
      
    lastResult: [],
    
    graphicsArray: [],
      
    xmlDoc: "",
      
    baseClass: 'jimu-widget-e911',
      
    bufferDistance: 250,
      
    panelHeight: "430px",
      
    labelLayer: null,
      
    resultsLayer: null,
      
/*{//////////////////////
Initialization functions
//////////////////////}*/
      
    postCreate: function() {
        this.inherited(arguments);
        console.log('postCreate');
        Request("widgets/FSMBGIS_E911Query/NineOneOneToolboxWidget.xml").then(lang.hitch(this,
        function(doc){
            this.processXmlDoc(doc)
        }));
        this.getListValues(this.addressLayerUrl, ["townVillageNameEng"]).then(lang.hitch(this, function(datastore) {
        this.populateList(datastore, this.OffBCtownVillageInput)
          }));
        this.getListValues(this.addressLayerUrl, ["cityGunNameEng"]).then(lang.hitch(this, function(datastore) { 
        this.populateList(datastore, this.OffBCcityInput);
          }));
        this.getListValues(this.addressLayerUrl, ["sectionNameEng"]).then(lang.hitch(this, function(datastore) { 
        this.populateList(datastore, this.OffBCsectionInput);
          }));
        this.openLocationChoiceScreen()
    },
      
    processXmlDoc: function(doc){
        //console.log("lanmarkListFunction");  
        //debugger;
        this.xmlDoc = xmlParser.parse(doc);
        console.log("Xml Coming")
        console.log(this.xmlDoc)
        this.fillSelectOptionsFromXmlNode(this.xmlDoc.getElementsByTagName("onbaselandmarktypes")[0],this.onBCLandmarkTypeDDL)
        this.fillSelectOptionsFromXmlNode(this.xmlDoc.getElementsByTagName("camps")[0],this.onBCInstallationDDL)
        this.fillSelectOptionsFromXmlNode(this.xmlDoc.getElementsByTagName("camps")[0],this.offBCStartInstallationTxt)
        //Note: I had to create the jurisdiction list from the XML file instead of the 
        this.fillSelectOptionsFromXmlNode(this.xmlDoc.getElementsByTagName("jurisdiction")[0],this.offBCJurisdictionDDL)
        //this.fillSelectOptionsFromXmlNode(this.xmlDoc.getElementsByTagName("wardsubdivision")[0],this.OffBCsectionInput)
        //this.fillSelectOptionsFromXmlNode(this.xmlDoc.getElementsByTagName("citydistrict")[0],this.OffBCcityInput)
        //this.fillSelectOptionsFromXmlNode(this.xmlDoc.getElementsByTagName("townvillage")[0],this.OffBCtownVillageInput)

        
    
    },
      
    fillSelectOptionsFromXmlNode: function (node, selectElement){
      //debugger
        node = node.getElementsByTagName("value")
        
        var listArray = []
        //console.log(node)
        for (var i = 0; i < node.length; i++){
            //console.log(node[i])
            listArray.push({"text":node[i].attributes.label.nodeValue, "value":node[i].attributes.data.nodeValue})
            
            if(node[i].attributes.type){
                if (node[i].attributes.type.nodeValue == "Section"){
                    if(this.sectionList.indexOf(node[i].attributes.label.nodeValue)<0)
                    this.sectionList.push(node[i].attributes.label.nodeValue)
                }
                if (node[i].attributes.type.nodeValue == "Gun"){
                    if(this.gunList.indexOf(node[i].attributes.label.nodeValue)<0){
                                this.gunList.push(node[i].attributes.label.nodeValue)
                        }
                }
                if (node[i].attributes.type.nodeValue == "City"){
                    if(this.cityList.indexOf(node[i].attributes.label.nodeValue)<0){
                                this.cityList.push(node[i].attributes.label.nodeValue)
                        }
                }
            }
        }
        this.populateList(listArray, selectElement)
        
    },
      
    getListValues: function(serviceURL, field){
        var def = new Deferred();
        var query = new Query();
        query.returnGeometry = false;
        query.returnDistinctValues = true;
        query.outFields = field;
        query.where = "1=1"
        var queryTask = new QueryTask(serviceURL);
        var text;
        var value;
        //debugger;
        if (field.length>1){
            text = field[0];
            value = field[1]
        } else{
            text = field[0];
            value = field[0];
        }
        //console.log(queryTask);
        queryTask.execute(query, lang.hitch(this, function(featureSet) {
          //console.log("query fired")
          var featureSetRemapped = [];
          //console.log(featureSet);
            //debugger
          for(var index in featureSet.features) {
            var feature = featureSet.features[index];            
            featureSetRemapped.push({
                text: feature.attributes[text],
                value: feature.attributes[value]
                
            });
          }
        def.resolve(featureSetRemapped)
      }))
    return def
    },
    
    populateList: function(list, input){
        var sortByProperty = function (prop) {
        return function (x,y){
            return ((x[prop] === y[prop]) ? 0 : ((x[prop] > y[prop]) ? 1 : -1));
        };
        };
        //console.log(list.data[0])
        list.sort(sortByProperty("text"))
        //console.log(list)
        for (var index in list){
            if (list[index].text == null){
                continue
            }
            var option = document.createElement('option')
            option.text = list[index].text
            option.value = list[index].value
            input.add(option)
        }
        
        //console.log(listArray)
        
    },
      
/*{//////////////////////////////
Update Spatial Filters
///////////////////////}*/
            
    updateSpatialFilter: function(e){
        
        if(e.srcElement.dataset.dojoAttachPoint == "onBCInstallationDDL"){
            if (this.onBCInstallationDDL.value == "Select an Installation"){
                this.spatialFilter = undefined
                return
            }
        this.getInstallationSpatialFilter(this.onBCInstallationDDL.value)
        }
        
        if(e.srcElement.dataset.dojoAttachPoint == "offBCJurisdictionDDL"){
            if(this.offBCJurisdictionDDL.value == "Select a Jurisdiction"){
                this.spatialFilter = undefined
                return
            }
        this.getJursidictionSpatialFilter(this.offBCJurisdictionDDL.value)
        }
    },
    
    getJursidictionSpatialFilter: function(inputValue){
        //var def = new Deferred();
        var spatialQueryParameters    
        //console.log("this")
        if(this.gunList.indexOf(inputValue) != -1){
           spatialQueryParameters = this.getQueryParameters("jurisdictionlayer","gunexpression",[["[jurisdictionvalue]",inputValue]])
           
           } else if(this.cityList.indexOf(inputValue) != -1){
               spatialQueryParameters = this.getQueryParameters("jurisdictionlayer","cityexpression",[["[jurisdictionvalue]",inputValue]])  
        } else if (this.sectionList.indexOf(inputValue) != -1){
            spatialQueryParameters = this.getQueryParameters("jurisdictionlayer","sectionexpression",[["[jurisdictionvalue]",inputValue]])            
        }
                  var serviceURL = spatialQueryParameters[0];
          var expression = spatialQueryParameters[1];
        
          var query = new Query();
          query.returnGeometry = true;
          query.where = expression;
          var queryTask = new QueryTask(serviceURL);
          queryTask.execute(query, lang.hitch(this, function(featureSet) {
              //debugger;
              if (featureSet.features.length > 0){
                  if(featureSet.features.length > 1){
                      var geometriesList = []
                      //debugger
                      for (var i in featureSet.features){
                          geometriesList.push(featureSet.features[i].geometry)
                      }
                      this.spatialFilter = geometryEngine.union(geometriesList)
                  }else{
              //def.resolve(new Memory({
                  //data: 
                  this.spatialFilter = featureSet.features[0].geometry}
              //}))
          }
              
          }));           

        
    },
      
    getInstallationSpatialFilter: function(inputValue){
        var def = new Deferred();
        var spatialQueryParameters

        spatialQueryParameters = this.getQueryParameters("camplayer", "expression", [["[campvalue]",this.onBCInstallationDDL.value]])
        var serviceURL = spatialQueryParameters[0];
        var expression = spatialQueryParameters[1];
        var query = new Query();
        query.returnGeometry = true;
        query.where = expression;
        var queryTask = new QueryTask(serviceURL);
        queryTask.execute(query, lang.hitch(this, function(featureSet) {
              //debugger;
              if (featureSet.features.length > 0){
                  if(featureSet.features.length > 1){
                      var geometriesList = []
                      //debugger
                      for (var i in featureSet.features){
                          geometriesList.push(featureSet.features[i].geometry)
                      }
                      debugger
                      this.spatialFilter = geometryEngine.union(geometriesList)
                  }else{
              //def.resolve(new Memory({
                  //data: 
                  this.spatialFilter = featureSet.features[0].geometry}
              //}))
        }
              
          }));
        return def
    },  
      
/*{/////////////////////
Query functions by button
Each Query calls get query parameters
then it calls Create List
//////////////////////}*/
   
    onBaseLandmarkDescriptionFind: function(e){
        var queryParameters
        if (this.onBCLandmarkDescriptionTxt.value != ""){
            
            if (this.onBCLandmarkTypeDDL.value != "Select a landmark type" && this.onBCInstallationDDL.value != "Select an Installation"){
                queryParameters = this.getQueryParameters("onbaselandmarklayer","expressionwithtypecamp",[["[value]", this.onBCLandmarkDescriptionTxt.value],['[campvalue]', this.onBCInstallationDDL.value],[ '[typevalue]', this.onBCLandmarkTypeDDL.value]])
                this.createList(queryParameters)
                return
            } else if (this.onBCLandmarkTypeDDL.value != "Select a landmark type"){
                queryParameters = this.getQueryParameters("onbaselandmarklayer","expressionwithtype",[["[value]", this.onBCLandmarkDescriptionTxt.value], [ '[typevalue]', this.onBCLandmarkTypeDDL.value]])
                this.createList(queryParameters)
                return
            } else if (this.onBCInstallationDDL.value != "Select an Installation"){
                queryParameters = this.getQueryParameters("onbaselandmarklayer","expressionwithcamp",[["[value]", this.onBCLandmarkDescriptionTxt.value],['[campvalue]', this.onBCInstallationDDL.value]])
                this.createList(queryParameters)
                return
            } else {
                queryParameters = this.getQueryParameters("onbaselandmarklayer","expression",[["[value]", this.onBCLandmarkDescriptionTxt.value]])
                this.createList(queryParameters)
                return
            }
        }
        
        if (this.onBCLandmarkTypeDDL.value != "Select a landmark type"){
            
            if (this.onBCLandmarkTypeDDL.value != "Select a landmark type" && this.onBCInstallationDDL.value != "Select an Installation"){
                queryParameters = this.getQueryParameters("onbaselandmarklayer","expressionwithtypecamp",[["[value]", this.onBCLandmarkDescriptionTxt.value],['[campvalue]', this.onBCInstallationDDL.value],[ '[typevalue]', this.onBCLandmarkTypeDDL.value]])
                this.createList(queryParameters)
                return                
            } else if (this.onBCInstallationDDL.value != "Select an Installation"){
                queryParameters = this.getQueryParameters("onbaselandmarklayer","expressionwithcamp",[["[value]", this.onBCLandmarkDescriptionTxt.value],['[campvalue]', this.onBCInstallationDDL.value]])
                this.createList(queryParameters)
                return
            } else {
                queryParameters = this.getQueryParameters("onbaselandmarklayer","expressionwithtype",[["[value]", this.onBCLandmarkDescriptionTxt.value], [ '[typevalue]', this.onBCLandmarkTypeDDL.value]])
                this.createList(queryParameters)
                return
                
            }
        }
   
        
    },
      
    onBaseRoadFind: function(){
        //Query on-base Roads - CALLER
        if (this.onBCRoad1Txt.value != "" && this.onBCRoad2Txt.value != "" && this.onBCInstallationDDL.value != "Select an Installation"){ this.createList(this.getQueryParameters("roadintersectionlayer","onbaseexpressionwithcamp",[["[value1]",this.onBCRoad1Txt.value],["[value2]",this.onBCRoad2Txt.value],["[campvalue]",this.onBCInstallationDDL.value]]))
                                                                                                                                     
        }
        if (this.onBCRoad1Txt.value != "" && this.onBCRoad2Txt.value != "" && this.onBCInstallationDDL.value == "Select an Installation"){ this.createList(this.getQueryParameters("roadintersectionlayer","onbaseexpression",[["[value1]",this.onBCRoad1Txt.value],["[value2]",this.onBCRoad2Txt.value]]))
        return
        }
        if (this.onBCRoad1Txt.value != "" && this.onBCRoad2Txt.value == ""){ this.createList(this.getQueryParameters("roadlayer","expression",[["[value1]",this.onBCRoad1Txt.value]]))
        return                                                                  
        }
        if (this.onBCRoad2Txt.value != "" && this.onBCRoad1Txt.value == ""){ this.createList(this.getQueryParameters("roadlayer","expression",[["[value1]",this.onBCRoad2Txt.value]]))
        return                                                                    
        }
       
    },

    BuildingNumFind: function(){
        var queryParameters
        
        //Query on-base Buidling numbers
        if (this.onBCBuildingNumTxt.value != ""){
            if (this.onBCInstallationDDL.value != "Select an Installation"){
                queryParameters = this.getQueryParameters("buildingnolayer", "expressionwithcamp", [["[value]", this.onBCBuildingNumTxt.value],["[campvalue]",this.onBCInstallationDDL.value]]);
                this.createList(queryParameters)
                return
            } else {
            queryParameters = this.getQueryParameters("buildingnolayer", "expression", [["[value]", this.onBCBuildingNumTxt.value]])
                this.createList(queryParameters)
                return
            }
        }
 
          
      },
      
    onMilitaryTrainingLocationFind: function(){
        //Query Training Location
        var queryParameters
        if (this.onBCMilitaryTrainingLocationTxt.value != ""){
            if (this.onBCInstallationDDL.value != "Select an Installation"){
                queryParameters = this.getQueryParameters("traininglocationlayer", "onbaseexpressionwithcamp", [["[value]",this.onBCMilitaryTrainingLocationTxt.value], ["campvalue", this.onBCInstallationDDL.value]])
                this.createList(queryParameters)
                return
            } else{
                //console.log(onBCInstallationDDL)
                queryParameters = this.getQueryParameters("traininglocationlayer", "onbaseexpression", [["[value]",this.onBCMilitaryTrainingLocationTxt.value],["[campvalue]",this.onBCInstallationDDL.value]])
                this.createList(queryParameters)
                return
              }
            }
    },
    
    BuildingNameFind: function(){
        var queryParameters
        //Query on-base building names
        if (this.onBCBuildingNameTxt.value != ""){
            if (this.onBCInstallationDDL.value != "Select an Installation"){
                queryParameters = this.getQueryParameters("buildingnamelayer", "expressionwithcamp", [["[value]", this.onBCBuildingNameTxt.value],["[campvalue]",this.onBCInstallationDDL.value]])
                this.createList(queryParameters)
                return
            } else {
                queryParameters = this.getQueryParameters("buildingnamelayer", "expression", [["[value]", this.onBCBuildingNameTxt.value]])
                this.createList(queryParameters)
                return
            }
            
        }

    },
    
    buildAddressString: function(){
        var addressValue = ""
        if (this.OffBCchrome.value != ""){
                addressValue += this.OffBCchrome.value}
            if (this.OffBCbranchi.value != ""){
                addressValue += "-" + this.OffBCbranchi.value}
            if (this.OffBCgou.value != "") {
                addressValue += "-" + this.OffBCgou.value}
            if (this.OffBChude.value !="" ){
                addressValue += "-" + this.OffBChude.value}
            
            return addressValue
        
    },
      
    offBaseAddressFind: function(){
        
        var finalExpression
        var re
        var townExpression
        var layerNode = this.xmlDoc.getElementsByTagName("addresslayer")
        var labeField = layerNode[0].getElementsByTagName("labelfield")[0].childNodes[0].nodeValue
        var serviceURL = layerNode[0].getElementsByTagName("url")[0].childNodes[0].nodeValue
        //debugger
        if(this.OffBCsectionInput.value != "Select a Ward/Subdivision"){
            finalExpression = layerNode[0].getElementsByTagName("sectionexpression")[0].childNodes[0].nodeValue
            re = new RegExp("\\[wardvalue]", "g")
            finalExpression =  finalExpression.replace(re, this.OffBCsectionInput.value)
            if(this.OffBCchrome.value != "" || this.OffBCbranchi.value != "" || this.OffBCgou.value != "" || this.OffBChude.value !=""){
            re = new RegExp("\\[addrvalue]", "g")
            finalExpression += " AND " + layerNode[0].getElementsByTagName("addrexpression")[0].childNodes[0].nodeValue.replace(re, this.buildAddressString())
            }
            if (this.OffBCcityInput.value != "Select a City/District"){
                var cityExpression = layerNode[0].getElementsByTagName("cityexpression")[0].childNodes[0].nodeValue;
                re = new RegExp("\\[cityvalue]", "g")
                finalExpression += " AND " + cityExpression.replace(re, this.OffBCcityInput.value);
            }
            if (this.OffBCtownVillageInput.value != "Select a Town/Villiage"){
                townExpression = layerNode[0].getElementsByTagName("townexpression")[0].childNodes[0].nodeValue;
                re = new RegExp("\\[townvalue]", "g")
                finalExpression += " AND " + townExpression.replace(re, this.OffBCtownVillageInput.value);
            }
            this.createList([serviceURL, finalExpression, labeField])
            return
        } else if (this.OffBCtownVillageInput.value != "Select a Town/Villiage"){
            finalExpression = layerNode[0].getElementsByTagName("townexpression")[0].childNodes[0].nodeValue;
            re = new RegExp("\\[townvalue]", "g")
            finalExpression = finalExpression.replace(re, this.OffBCtownVillageInput.value);
            
            if(this.OffBCchrome.value != "" || this.OffBCbranchi.value != "" || this.OffBCgou.value != "" || this.OffBChude.value !=""){
                re = new RegExp("\\[addrvalue]", "g")
            finalExpression += " AND " + layerNode[0].getElementsByTagName("addrexpression")[0].childNodes[0].nodeValue.replace(re, this.buildAddressString())
            }
            
            if (this.OffBCcityInput.value != "Select a City/District"){
                var cityExpression = layerNode[0].getElementsByTagName("cityexpression")[0].childNodes[0].nodeValue;
                re = new RegExp("\\[cityvalue]", "g")
                finalExpression += " AND " + cityExpression.replace(re, this.OffBCcityInput.value);
            }
            this.createList([serviceURL, finalExpression, labeField])
            return
        } else if (this.OffBCcityInput.value != "Select a City/District"){
            finalExpression = layerNode[0].getElementsByTagName("cityexpression")[0].childNodes[0].nodeValue;
            re = new RegExp("\\[cityvalue]", "g")
            finalExpression = finalExpression.replace(re, this.OffBCcityInput.value);
            if(this.OffBCchrome.value != "" || this.OffBCbranchi.value != "" || this.OffBCgou.value != "" || this.OffBChude.value !=""){
                re = new RegExp("\\[addrvalue]", "g")
            finalExpression += " AND " + layerNode[0].getElementsByTagName("addrexpression")[0].childNodes[0].nodeValue.replace(re, this.buildAddressString())
            }
            this.createList([serviceURL, finalExpression, labeField])
            return
        } else if (this.OffBCchrome.value != "" || this.OffBCbranchi.value != "" || this.OffBCgou.value != "" || this.OffBChude.value !=""){
            
             re = new RegExp("\\[addrvalue]", "g")
            finalExpression = layerNode[0].getElementsByTagName("addrexpression")[0].childNodes[0].nodeValue.replace(re, this.buildAddressString())
            this.createList([serviceURL, finalExpression, labeField])
            return
        } else{
            alert("Address, ward/subdivision, town/villiage, or city/district values must be chosen/entered to continue.")
        }
    },
    
    offBaseRoadFind: function(){
        //Query off base Road Intersection
        if (this.offBCRoad1Txt.value != "" && this.offBCRoad2Txt.value != ""){
            this.createList(this.getQueryParameters("roadintersectionlayer","offbaseexpression",[["[value1]",this.offBCRoad1Txt.value],["[value2]",this.offBCRoad2Txt.value]]))
            return
        }
        //Query off base Road
        if (this.offBCRoad1Txt.value != "" && this.offBCRoad2Txt.value == ""){ this.createList(this.getQueryParameters("roadlayer","expression",[["[value1]",this.offBCRoad1Txt.value]]))
        return                                                                      
        }
        if (this.offBCRoad1Txt.value == "" && this.offBCRoad2Txt.value != ""){ this.createList(this.getQueryParameters("roadlayer","expression",[["[value1]",this.offBCRoad2Txt.value]]))
        return                                                                      
        }
    },
      
    offBCStartGateFind: function(){
                if (this.offBCStartInstallationTxt.value != "Select an Installation" && this.offBCStartGateTxt.value !="") {
            this.createList(this.getQueryParameters("gatelayer", "expression", [["[campvalue]",this.offBCStartInstallationTxt.value],["[value]",this.offBCStartGateTxt.value]]))
        return} 
        if (this.offBCStartInstallationTxt.value != "Select an Installation" && this.offBCStartGateTxt.value ==""){alert("A gate must be entered to continue")}
        if (this.offBCStartInstallationTxt.value == "Select an Installation" && this.offBCStartGateTxt.value !="") {
                alert("An installation must be chosen to continue")
            }
        
    },
      
    offBCLandmarkDescriptionFind: function(){
                if (this.offBCLandmarkDescriptionTxt.value != ""){
                    this.landmarkSearch = true
                    this.createList(this.getQueryParameters("offbaselandmarklayer","expression",[["[value]", this.offBCLandmarkDescriptionTxt.value]]))
                    return
                } else {
                    alert("A landmark description must be entered to continue")
                }
    },
      
    offBCLandmarkRoadFind: function(){
             if (this.offBCLandmarkRoadTxt.value != ""){
                 this.landmarkSearch = true
                 this.createList(this.getQueryParameters("roadlayer", "expression",[["[value]", this.offBCLandmarkRoadTxt.value]]))
             } else {
                 alert("A road must be entered to continue")
             }
    },
      
    offBCLandmarkIntersectionFind:  function(e){
        if (this.offBCLandmarkIntersectionTxt.value !=""){
            this.landmarkSearch = true
            this.createList(this.getQueryParameters("offbaselandmarklayer","expressionintersection",[["[value]",this.offBCLandmarkIntersectionTxt.value]]))
            return
        } else {
            alert("A landmark intersection must be entered to continue")
        }
    }, 
      
/*{///////////////////////////
Query Functions
Each Query calls get query parameters
then it calls Create List
Create list calls get datastore
GetDatastore executes the query and calls resolve deffered
ResolveDeffered adds query results to the map
and resolves a deffered object to getDatastore ai waiting for
GetDatastore then passes the resolved Deffered 
object to createOnDemandList
///////////////////////////}*/
      
    getQueryParameters: function(layerNodeName, expressionName, valueArray){
          //console.log(layerNodeName);
          //console.log(expressionName);
          //console.log(valueArray);
        
          var layerNode = this.xmlDoc.getElementsByTagName(layerNodeName);
          //console.log(layerNode);
          var serviceURL = layerNode[0].getElementsByTagName("url")[0].childNodes[0].nodeValue;
          var expression = layerNode[0].getElementsByTagName(expressionName)[0].childNodes[0].nodeValue;
          for (var i = 0; i < valueArray.length; i++){
              //console.log(valueArray[i][0]);
              //console.log(valueArray[i][1]);
              var re = new RegExp("\\" + valueArray[i][0], "g")
              //console.log(re)
              expression = expression.replace(re, valueArray[i][1]);
          }
          
          //var labeField = layerNode[0].getElementsByTagName("labelfield")[0].childNodes[0].nodeValue;
        
         var labeField = ""
          if(layerNode[0].getElementsByTagName("labelfield")[0] != undefined){
              labeField = layerNode[0].getElementsByTagName("labelfield")[0].childNodes[0].nodeValue;
          } else {
          labeField = "OBJECTID"
          }
          //var queryField = ""
          //console.log(serviceURL);
          //console.log(expression);
          //console.log(labeField);
          return [serviceURL, expression, labeField];
      },

    //clears list, calls getDataStore then takes that result
    //and calls createOndemandList
    createList: function(queryParameters) {
        this.clearList();
        this.getDataStore(queryParameters).then(lang.hitch(this, function(datastore){ this.createOnDemandList(datastore)}));  
      },

    //Parses query parameters, builds query object,
    //Executes Query, calls resolveDeffered Query
    getDataStore: function(queryParameters){
        var def = new Deferred();
        var serviceURL = queryParameters[0];
        var expression = queryParameters[1];
        var labelField  = queryParameters[2];
        var query = new Query();
        query.returnGeometry = true;
        if(serviceURL == this.streetLayer){
            query.outFields = ['OBJECTID',"Street1", "Street2","Street3","Street4","Street5","Street6"]
        } else if (labelField == "OBJECTID"){
            query.outFields = ['OBJECTID']
        } else {
        query.outFields = ['OBJECTID', labelField]
        }
        //query.text= variableText;, 
        query.where = expression;
        this.featureLayer = new FeatureLayer(serviceURL);
        var queryTask = new QueryTask(serviceURL);
        if(this.spatialFilter){
            query.geometry = this.spatialFilter 
        }
        queryTask.execute(query, lang.hitch(this, function(featureSet){this.resolveDefferedQuery(featureSet, labelField, def)}));
        return def;
    },

    //Adds geometry to the map, creates datastore and resolves to deffered object
    //called by data store, returns promise to createlist
    resolveDefferedQuery: function(featureSet, labelField,  def) {
        if(featureSet.features.length == 0){
            alert("No Candidate Found")
        } else {
            var featureSetRemapped =[]
            this.graphicsArray = []
            if (this.resultsLayer){
                this.resultsLayer.clear()
            }
            //create buffer
            if (this.landmarkSearch && this.lastResult.length >0){//(this.lastResult.length >0){
                var featureSetGeometriesBufferArray = []
                //fill feature buffer array
                for (var index in featureSet.features){
                    var feature = featureSet.features[index];
                    featureSetGeometriesBufferArray.push(feature.geometry)
                }
                //create a buffer from the new query results to remove entries from last query that are outside of the
                //buffer area of the new query
                var resultsFilter = geometryEngine.buffer(featureSetGeometriesBufferArray, [this.bufferDistance], "meters", true)
                //remove entries in the last result that are not 
                //in buffer area of the current query
                for (var i = 0; i< this.lastResult.length;i++){
                    if(!geometryEngine.intersects(resultsFilter[0],this.lastResult[i].geometry)){
                        //if the item is not in the spatial filter remove from array
                        this.lastResult.splice(i,1);
                        i--
                    }
                }
                //concatenate the reduced version of the last result datastore
                //with the empty featureSetRemapped array
                featureSetRemapped = this.lastResult.concat(featureSetRemapped)
            }                  
            //fill featureSetRemapped with the resulting features
            for(var index in featureSet.features) {
                  var feature = featureSet.features[index];
                  //Custom label for Street intersections
                  if(featureSet.fieldAliases.Street1){
                      var roadArray = []//"Street1", "Street2","Street3","Street4","Street5","Street6"
                      if (feature.attributes["Street1"] && roadArray.indexOf(feature.attributes["Street1"])<0){
                        roadArray.push(feature.attributes["Street1"])
                      }
                      if (feature.attributes["Street2"] && roadArray.indexOf(feature.attributes["Street2"])<0){
                        roadArray.push(feature.attributes["Street2"])
                      }
                      if (feature.attributes["Street3"] && roadArray.indexOf(feature.attributes["Street3"])<0){
                        roadArray.push(feature.attributes["Street3"])
                      }
                      if (feature.attributes["Street4"] && roadArray.indexOf(feature.attributes["Street4"])<0){
                        roadArray.push(feature.attributes["Street4"])
                      }
                      if (feature.attributes["Street5"] && roadArray.indexOf(feature.attributes["Street5"])<0){
                        roadArray.push(feature.attributes["Street5"])
                      }
                      if (feature.attributes["Street6"] && roadArray.indexOf(feature.attributes["Street6"])<0){
                        roadArray.push(feature.attributes["Street6"])
                      }
                      //add a street feature to the featureSetRemapped array 
                      featureSetRemapped.push({
                          'id': feature.attributes["OBJECTID"],
                          'title': roadArray.join(" & "),
                          'geometry': feature.geometry,
                          'isLandmarkSearch': this.landmarkSearch
                      });
                      //if not a street feature add to featureSetRemapped
                  } else {
                      featureSetRemapped.push({
                          'id': feature.attributes["OBJECTID"],
                          'title': feature.attributes[labelField],
                          'geometry': feature.geometry,
                          'isLandmarkSearch': this.landmarkSearch
                      });
                  }
            }//end adding features to featureSetRemapped
            this.addDatastoreToMapAsGraphics(featureSetRemapped)
        }
        def.resolve(new Memory({
            data: featureSetRemapped
        }));
    },
   
    createOnDemandList: function(datastore) {
        this.ListNodeVar = new (declare([OnDemandList, Selection]))({
            'store': datastore,
            'selectionMode': 'single',
            'renderRow': lang.hitch(this, function (object) {
                return this._createListItem(object);
            })
        }, this.ListNode);
        this.ListNodeVar.startup();
        
        //Event handler for list item on click event
        this.ListNodeVar.on('.dgrid-row:click', lang.hitch(this, function(evt) {
            var row = this.ListNodeVar.row(evt.selectorTarget);
            if (this.itemSelected){
                this.resultsLayer.remove(this.resultsLayer.graphics[this.resultsLayer.graphics.length-1])
                this.resultsLayer.remove(this.resultsLayer.graphics[this.resultsLayer.graphics.length-1])
            }
            //Set buffer symbol properties for when a value is clicked
            this.itemSelected = true;
            var bufferSymbol
            switch(row.data.geometry.type){
                case 'point':
                    bufferSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 100, null, this.symbolColor50);
                    break;
                case 'polyline':
                    bufferSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, this.symbolColor50, 20);
                    break;
                case 'polygon':
                    bufferSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, this.symbolColor50, 20), this.symbolColor50);
                    break;
            }
            var graphic = new Graphic(row.data.geometry, bufferSymbol)
            var pinGraphic = new Graphic(row.data.geometry, this.defaultSymbol)
            graphic.setInfoTemplate(new InfoTemplate({title: "Search Result", content: row.data.title}))
            this.resultsLayer.add(graphic);
            this.resultsLayer.add(pinGraphic);
            var newMapExtent
            var geometry = row.data.geometry;
            var extent = geometry.getExtent();
            //shape = feature.getShape();
            if (extent && extent.getCenter) {
                newMapExtent = extent.getExtent(); // polygon & polyline
            } else {
                var zoomBuffer = 750
                newMapExtent = new Extent(geometry.x-zoomBuffer, geometry.y-zoomBuffer,geometry.x+zoomBuffer,  geometry.y+zoomBuffer,  this.map.spatialReference)
            }
            this.map.setExtent(newMapExtent); // move to the feature
        }));
        DojoQuery(".jimu-panel")[0].style.height = "600px"
        this.ListNode.style.height = "200px"
        //Now that query is done, reset landmark search
        this.landmarkSearch = false 
        //this should be the last function called
    },
   
    addDatastoreToMapAsGraphics: function(featureSetRemapped){
        var font = new Font("15px", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "Arial");
        if(!this.labelLayer){
            this.labelLayer = new GraphicsLayer({id: "labelLayer"})
        }
        if(!this.resultsLayer){
            this.resultsLayer = new GraphicsLayer({id: "resultsLayer"})
        }
        this.labelLayer.setMinScale(10000)
        for(var i in featureSetRemapped){
            var symbol;
            //define a symbol for the graphic based on geometry
            switch(featureSetRemapped[i].geometry.type){
                case 'point':
                    symbol = this.defaultSymbol;//new PictureMarkerSymbol("widgets/FSMBGIS_E911Query/images/i_pin.png", 51, 51)
                    break;
                case 'polyline':
                    symbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, '#e74c3c', 3);
                    break;
                case 'polygon':
                    symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, '#fff', 2), '#e74c3c');
                    break;
            }
            //add the current graphic to the map
            var infoTemplate = new InfoTemplate()
            infoTemplate.setTitle("Search Result")            
            infoTemplate.setContent(featureSetRemapped[i].title)            
            var resultGraphic = new Graphic(featureSetRemapped[i].geometry, symbol)
            resultGraphic.setInfoTemplate(infoTemplate)
            this.resultsLayer.add(resultGraphic);
            this.graphicsArray.push(new Graphic(featureSetRemapped[i].geometry, symbol));
            var textSymbol = new TextSymbol(featureSetRemapped[i].title,font, new Color([0, 0, 0]));
            textSymbol.setHaloSize(1)
            textSymbol.setHaloColor(new Color('#eee'))
            
         
            var pnt = new Point(featureSetRemapped[i].geometry)
            pnt.setY(featureSetRemapped[i].geometry.y-10)
            this.labelLayer.add(new Graphic(pnt, textSymbol))
          
            
        }
        this.map.addLayer(this.resultsLayer)
        this.map.addLayer(this.labelLayer)
        
        
        //featureSetRemapped.isLandmarkSearch = false
        if (this.landmarkSearch){
            this.RefineSearchBtn.style.display = ""
            this.spatialFilter = geometryEngine.buffer(GraphicsUtils.getGeometries(this.graphicsArray), [this.bufferDistance], "meters", true)[0]
            this.lastResult = featureSetRemapped
            //featureSetRemapped.isLandmarkSearch = true
        } else{
            this.lastResult = []
        }
        var newExtent = GraphicsUtils.graphicsExtent(this.graphicsArray)
        var zoomBuffer = 750
        var newMapExtent = new Extent(newExtent.xmin-zoomBuffer, newExtent.ymin-zoomBuffer,newExtent.xmax+zoomBuffer,  newExtent.ymax+zoomBuffer,  this.map.spatialReference)
        this.map.setExtent(newMapExtent)
        //console.log(featureSetRemapped)
        this.CandidateCount.innerHTML = featureSetRemapped.length.toString() + " Candidates found:"
    },
    //Create html for list item 
    _createListItem: function(featureObj) {
      var listItemRoot = document.createElement('DIV');
      listItemRoot.className = 'list-item';
      /*var thumbnailImg = document.createElement("img")
      thumbnailImg.src = "widgets/E911Query/images/resultRecord.bmp"
        //thumbnailImg.display = "inline"
        listItemRoot.appendChild(thumbnailImg)
        
       var spanText = document.createElement('span');
        spanText.style.color="#2470c2"
        spanText.innerHTML = "●  "
        listItemRoot.appendChild(spanText)
        */
        if (featureObj.isLandmarkSearch){
                      var checkBox = document.createElement("input")
        checkBox.type = "checkbox"
        checkBox.id = "dgridCheckbox_" + featureObj.id
        listItemRoot.appendChild(checkBox)                  
                  } else {
                      var spanText = document.createElement('span');
        spanText.style.color="#2470c2"
        spanText.innerHTML = "●  "
        listItemRoot.appendChild(spanText)
                  }
        
      if(featureObj) {
        var listItemTitle;
        // Create title
        //console.log(featureObj.title)
        if(featureObj.title){ //&& typeof featureObj.title === 'string') {
          listItemTitle = document.createElement('span');
          listItemTitle.innerHTML = featureObj.title;
          listItemRoot.appendChild(listItemTitle);
          
        }
          
          
      } else {
        listItemRoot.innerHTML = 'NO DATA AVAILABLE';
      }

      return listItemRoot;
    },
      
    refineLandmarkSearch: function(){
        
          var checkItems = DojoQuery("input:checked")
          var objectIds = []
          var datastore  = new Memory()
          console.log(checkItems)
        console.log(checkItems.length)
          for(var i = 0; i < checkItems.length; i++){
              objectIds.push(parseInt(checkItems[i].id.split("_")[1]))
          }
        //console.log(objectIds)
        for(var i = 0; i < this.lastResult.length; i++){
            if(objectIds.indexOf(this.lastResult[i].id)>-1){
                datastore.put(this.lastResult[i])
            }
        }
        this.clearList();
        this.landmarkSearch = true
        this.addDatastoreToMapAsGraphics(datastore.data)
        this.landmarkSearch = false
        this.createOnDemandList(datastore)
        
      },
          
    clearList: function(){  
      this.ListNode.innerHTML = ""
      this.ListNode.className = ""
      this.ListNode.id = ""
      this.CandidateCount.innerHTML = ""
        if (this.resultsLayer){
            this.resultsLayer.clear()
        }
        if (this.labelLayer){
            this.labelLayer.clear()
        }
      this.itemSelected = false; 
      this.graphicsArray = [];
      this.map.infoWindow.hide()
      this.RefineSearchBtn.style.display = "none"
      DojoQuery(".jimu-panel")[0].style.height = this.panelHeight
    },
      
    clearAll: function(){
        //Clear Input values
          DojoQuery("input").forEach(function(n){
              n.value = "";
          });
          //Reset selection values
         //debugger
          DojoQuery("select").forEach(function(n){
             //console.log(n)
            if (n.classList.contains("jimu-select-installation")){
                n.value = "Select an Installation"
            } else if (n.classList.contains("jimu-select-landmark")){
                n.value = "Select a landmark type"
            } else if (n.classList.contains("jimu-select-ward")){
                n.value = "Select a Ward/Subdivision"
            
            } else if (n.classList.contains("jimu-select-town")){
                n.value = "Select a Town/Villiage"
            } else if (n.classList.contains("jimu-select-city")){
                n.value = "Select a City/District"
            } else if (n.classList.contains("jimu-widget-address-jurisdiction")){
                n.value = "Select a Jurisdiction"
            } 
                    })
        this.clearList()
        this.spatialFilter = null;
        this.landmarkSearch = false;
        this.lastResult = []; 
    },
      
    locationChoice: function(e){
        //console.log(onBaseTC)
        //console.log(e);
        //debugger
        if(e.srcElement.dataset.dojoAttachPoint == "onBaseQueryButton"){
            if(!this.onBaseTC){this.startOnbaseTabContainer()}
            this.searchTitle.innerHTML = "<span style='font-size: 15px;font-weight: bold;'>On-Base Incident</span>"
            this.onBaseTab.style.display = ""
            this.onBaseTabCaller.style.display = ""
            //this.onBaseTabResponder.style.display = "none"
            this.submitQuery_ResultsPane.style.display = ""
            this.openLocationScreen.style.display = ""
            this.locationChoiceScreen.style.display = "none"
            
            //console.log(this.onBaseTab.style)
        } else if(e.srcElement.dataset.dojoAttachPoint == "offBaseQueryButton"){
            if(!this.offBaseTC){this.startOffBaseTabContainer()}
            this.searchTitle.innerHTML = "<span style='font-size: 15px;font-weight: bold;'>Off-Base Incident</span>"
            this.offBaseTab.style.display = "";
            this.offBaseTabCaller.style.display = ""
            //this.offBaseTabResponder.style.display = "none"
            this.submitQuery_ResultsPane.style.display = ""
            this.openLocationScreen.style.display = ""
            this.locationChoiceScreen.style.display = "none"
            
            
        }
    },
      
    openLocationChoiceScreen: function(){
        this.searchTitle.innerHTML = ""
        this.onBaseTab.style.display = "none"
        this.offBaseTab.style.display = "none";
        this.submitQuery_ResultsPane.style.display = "none";
        this.openLocationScreen.style.display = "none";
        this.locationChoiceScreen.style.display = "";
        this.RefineSearchBtn.style.display = "none"
  },
     
    startup: function() {
      this.inherited(arguments);
    },
    
    keyListener: function(e){
          if(e.keyCode==13){
              switch(e.srcElement.dataset.dojoAttachPoint){
                  case "onBCBuildingNameTxt":
                      this.BuildingNameFind()
                      break;
                  case "onBCBuildingNumTxt":
                      this.BuildingNumFind()
                      break;
                  case "onBCLandmarkDescriptionTxt":
                      this.onBaseLandmarkDescriptionFind()
                      break;
                  case "onBCMilitaryTrainingLocationTxt":
                      this.onMilitaryTrainingLocationFind()
                      break;
                  case "onBCRoad1Txt":
                 case "onBCRoad2Txt":
                      this.onBaseRoadFind()
                      break;
                 case "onBCLatTxt":
                      this.dropPin()
                      break;
                 case "onBCLongTxt":
                      this.dropPin()
                      break;
                 case "offBCPane":
                 case "OffBCchrome":
                 case "OffBCbranchi":
                 case "OffBCgou":
                 case "OffBChude":
                      this.offBaseAddressFind()
                      break;
                 case "offBCLandmarkDescriptionTxt":
                      this.offBCLandmarkDescriptionFind()
                      break;
                 case "offBCLandmarkRoadTxt":
                      this.offBCLandmarkRoadFind()
                      break;
                 case "offBCLandmarkIntersectionTxt":
                      this.offBCLandmarkIntersectionFind()
                      break;
                 case "offBCRoad1Txt":
                 case "offBCRoad2Txt":
                      this.offBaseRoadFind()
                      break;
                 case "offBCStartGateTxt":
                      this.offBCStartGateFind()
                      break;
                 case "offBCLatTxt":
                 case "offBCLongTxt":
                      this.dropPin
                                            
              }
          }
      },
      
    startOnbaseTabContainer: function(){
/*        declare("QueryManager",[_WidgetBase, _TemplateMixin, TabContainer, ContentPane, attr, DojoQuery, AccordionContainer],{
            templateString: onBaseTabtemplate,
        
                                                                                    
       postCreate: function(){DojoQuery(".tc1cp").forEach(function(n){
        new ContentPane({
            // just pass a title: attribute, this, we're stealing from the node
            title: attr.get(n, "title")
        }, n);
        });
        onBaseTabContainer = new TabContainer({
            style: attr.get("onBaseTab", "style")
        }, "onBaseTab");
        onBaseTabContainer.startup();
        console.log(onBaseTabContainer)
                             }
        })*/
        //on Base Caller panes
        this.onBaseTC = true
        DojoQuery(".onBCPane").forEach(function(n){
            new ContentPane({
                // just pass a title: attribute, this, we're stealing from the node
                title: attr.get(n, "title")
            }, n);
        });
        var ac1 = new AccordionContainer({
            style: attr.get("ta1-prog", "style")
        }, "ta1-prog");
        ac1.startup();
        //on Base Responder Panes

        
        //this.currentTabContainer = onBaseTabContainer
        //console.log(onBaseTabContainer)
    },
          
    startOffBaseTabContainer: function(){
        //off base tabs
/*        DojoQuery(".tc2cp").forEach(function(n){
            new ContentPane({
                // just pass a title: attribute, this, we're stealing from the node
                title: attr.get(n, "title")
            }, n);
        });
        offBaseTabContainer = new TabContainer({
            style: attr.get("offBaseTab", "style")
        }, "offBaseTab");
        offBaseTabContainer.startup();*/
        //off base Caller panes
        this.offBaseTC = true
        DojoQuery(".offBCPane").forEach(function(n){
            new ContentPane({
                // just pass a title: attribute, this, we're stealing from the node
                title: attr.get(n, "title")
            }, n);
        });
        var ac1 = new AccordionContainer({
            style: attr.get("ta3-prog", "style")
        }, "ta3-prog");
        ac1.startup();
        //off base Responder Panes
    

      },
      
    onOpen: function(){

        
        //this.startOnbaseTabContainer();
        //this.startOffBaseTabContainer();
        console.log('onOpen');
        DojoQuery(".jimu-panel")[0].style.height = this.panelHeight
        
        //console.log(xmlDoc.getElementsByTagName("onbaselandmarktypes"))
        
        //this.openLocationChoiceScreen()
        //console.log(valueList)
    
    },

    dropPin: function(e){
        var projectionPromise = Projection.load()
        var latY = ""
        var lonX = ""
        var alertMessage = "Both Latttitude and Longitude must be entered"
        if(e.srcElement.dataset.dojoAttachPoint == "onBCDropPinBtn"){
            if(this.onBCLatTxt.value == "" || this.onBCLongTxt.value == ""){
                alert(alertMessage)
                return
            } else {
                lonX = this.onBCLongTxt.value
                latY = this.onBCLatTxt.value
            }
            
        }
       
        if(e.srcElement.dataset.dojoAttachPoint == "offBCDropPinBtn"){
            if(this.offBCLatTxt.value == "" || this.offBCLongTxt.value == ""){
                alert(alertMessage)
                return
            } else {
                lonX = this.offBCLongTxt.value
                latY = this.offBCLatTxt.value
            }
            
        }

        if(parseFloat(latY) && parseFloat(lonX)){
            latY = parseFloat(latY)
            lonX = parseFloat(lonX)
            if(latY>-90 && latY<90){
                if(lonX>-180 && lonX<180){
                    //Main Body
                    //Create new graphic and symbolize
                    /*var newPin = {"geometry":{"x":lonX,"y":latY, "spatialReference":{"wkid":4326}},"symbol":{"color":[255,255,255,64],
    "size":12,"angle":0,"xoffset":0,"yoffset":0,"type":"esriSMS","style":"esriSMSCircle",
    "outline":{"color":[0,0,0,255],"width":1,"type":"esriSLS","style":"esriSLSSolid"}}
                                 }*/
                    //debugger;
                    var newPin = new Point(lonX,latY)
                    //var graphic = new Graphic(newPin) 
                    projectionPromise.then(lang.hitch(this, function(){
                        //debugger
                      
                        var rePin = Projection.project(newPin, this.map.spatialReference)
                        //var symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 20, null, '#e74c3c');
                        var graphic = new Graphic(rePin, this.defaultSymbol)
                        
                        console.log(graphic)
                        
                        this.resultsLayer.add(graphic)
                        this.map.centerAt(rePin)
                    }));
                    //add graphic to grapic layer
                     //this.resultsLayer.add(graphic)
                     //var geometry = graphic.geometry
                     
                    //Projection.project(graphic.geometry, this.map.spatialReference)
                    
                     //this.map.centerAt()
                    
                    
                    
                } else{
                    alert("Longitude is not within accpetable range of values")
                }
            } else{
                alert("Lattitude is not within accpetable range of values")
            }
        } else {
            alert("Either lattitude or logitude is not a valid number")
        }  
    },
          
    onClose: function(){
      console.log('onClose');
    },

    onMinimize: function(){
      console.log('onMinimize');
    },

    onMaximize: function(){
      console.log('onMaximize');
    },

    onSignIn: function(credential){
      /* jshint unused:false*/
      console.log('onSignIn');
    },

    onSignOut: function(){
      console.log('onSignOut');
    },
      
    updateAddressOptions: function(event){
          
      },
      
  });
});