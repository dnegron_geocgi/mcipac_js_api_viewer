define({
	root: ({
		headerText: "Set header text:",
        selectLayerText: "Select a layer:",
        pickAttributeText: "Map the following UI component to attributes:",
        UIElementNames: {
            thumbnail: "thumbnail",
            title: "title"
        }
  })
});
