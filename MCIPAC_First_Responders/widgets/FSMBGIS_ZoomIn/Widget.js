define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'jimu/BaseWidget',
    'dojo/_base/html',
    "esri/toolbars/navigation",
    'dojo/on'
  ],
  function(declare, lang, BaseWidget, html, navigation, on) {
    var clazz = declare([BaseWidget], {
      name: 'ZoomSlider',

      baseClass: 'jimu-widget-zoomslider',

      _disabledClass: 'jimu-state-disabled',

      moveTopOnActive: false,

      postCreate: function(){
        this.inherited(arguments);
        this.btnZoomIn.title = window.jimuNls.common.zoomIn;
        this.navToolBar = new navigation(this.map)
      },
    

      setPosition: function(position){
        this.inherited(arguments);
      },

      _onBtnZoomInClicked: function(){
        this.toolTip_Init();
        this.navToolBar.activate(navigation.ZOOM_IN);
        this.deactivateZoomIn = this.map.on("extent-change", lang.hitch(this, function() {
              this.navToolBar.deactivate(navigation.ZOOM_IN)
              this.deactivateZoomIn.remove()
              this.mapDiv.removeChild(this.toolTip)
          }));
      },
        toolTip_Init: function(){
        this.mapDiv = document.getElementById("map")
        this.toolTip = document.createElement('SPAN')
        this.toolTip.classList.add("dijitTooltipContainer")
        this.toolTip.style.position ="fixed"
        this.toolTip.innerHTML = "Click and drag on map to zoom in"
        //this.clickMapTip = 
        //var mapDiv = document.getElementById("map")
        this.mapDiv.appendChild(this.toolTip)
        this.map.on("mouse-move", lang.hitch(this, function(evt) {
            var px, py;
            //console.log(evt)
            if (evt.clientX || evt.pageY) {
              px = evt.clientX;
              py = evt.clientY;
            } else {
                this.toolTip.style.display = "none";
              //px = evt.clientX + dojo.body().scrollLeft - dojo.body().clientLeft;
              //py = evt.clientY + dojo.body().scrollTop - dojo.body().clientTop;
            }
                           
            // dojo.style(tooltip, "display", "none");
            
            this.toolTip.style.display = "none";
            this.toolTip.style.left = (px + 15) + "px"
            this.toolTip.style.top = (py + 15) + "px" ;
            // dojo.style(tooltip, "display", "");
            this.toolTip.style.display = "";
            //console.log(this.toolTip)
            
            // console.log("updated tooltip pos.");
          }));
        this.map.on("mouse-out",lang.hitch(this, function() {
            this.toolTip.style.display = "none";
        }))
    },
        
    });
    return clazz;
  });