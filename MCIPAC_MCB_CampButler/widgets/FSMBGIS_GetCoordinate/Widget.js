define(['dojo/_base/declare',
        'jimu/BaseWidget',
        "esri/geometry/projection",
        'esri/geometry/webMercatorUtils',
        'esri/geometry/coordinateFormatter',
        'esri/tasks/ProjectParameters',
        'esri/tasks/GeometryService',
        "esri/SpatialReference",
        "esri/symbols/PictureMarkerSymbol",
        "esri/symbols/SimpleMarkerSymbol",
        'esri/graphic',
        "esri/geometry/Extent",
        'dojo/on',
        'dojo/query', 
        "esri/layers/GraphicsLayer",
        "esri/symbols/Font",
        "esri/symbols/TextSymbol",
        "esri/geometry/Point",
        "dojo/_base/Color",
       'dojo/_base/lang'],
function(declare, BaseWidget, projection, webMercatorUtils, coordinateFormatter, ProjectParameters, GeometryService, SpatialReference, PictureMarkerSymbol, SimpleMarkerSymbol, graphic, Extent, on, query, GraphicsLayer, Font, TextSymbol, Point, Color, lang) {
  //To create a widget, you need to derive from BaseWidget.
  return declare([BaseWidget], {
 
    baseClass: 'jimu-widget-demo',
      
    getCoordinate: "",
    
    pointsDropped: 0,
    
    symbolColor50: [196,252,255,0.5],
      
    itemSelected: false,
      
    geometryService: "https://devserver.local/server/rest/services/Utilities/Geometry/GeometryServer",
      
    defaultSymbol: new PictureMarkerSymbol({"url":"widgets/FSMBGIS_GetCoordinate/images/pin.png",
                                            "height":51,
                                            "width":51,
                                            "yoffset":25,
                                            "xoffset":20
                                           }),
      
    nodeTemplate: "Coordinate X: {pointX} <br> Coordinate Y: {pointY}",
      
    labelLayer: new GraphicsLayer({id: "coordinateLabelLayer"}),
      
    coordinateLayer: new GraphicsLayer({id: "coordinateResultsLayer"}),
      
    toolTip_Init: function(){
        this.mapDiv = document.getElementById("map")
        this.toolTip = document.createElement('SPAN')
        this.toolTip.classList.add("dijitTooltipContainer")
        this.toolTip.style.position ="fixed"
        this.toolTip.innerHTML = "Click to get a coordinate"
        //this.clickMapTip = 
        //var mapDiv = document.getElementById("map")
        this.mapDiv.appendChild(this.toolTip)
        this.map.on("mouse-move", lang.hitch(this, function(evt) {
            var px, py;
            //console.log(evt)
            if (evt.clientX || evt.pageY) {
              px = evt.clientX;
              py = evt.clientY;
            } else {
                this.toolTip.style.display = "none";
              //px = evt.clientX + dojo.body().scrollLeft - dojo.body().clientLeft;
              //py = evt.clientY + dojo.body().scrollTop - dojo.body().clientTop;
            }
                           
            // dojo.style(tooltip, "display", "none");
            
            this.toolTip.style.display = "none";
            this.toolTip.style.left = (px + 15) + "px"
            this.toolTip.style.top = (py + 15) + "px" ;
            // dojo.style(tooltip, "display", "");
            this.toolTip.style.display = "";
            //console.log(this.toolTip)
            
            // console.log("updated tooltip pos.");
          }));
        this.map.on("mouse-out",lang.hitch(this, function(evt) {
            this.toolTip.style.display = "none";
        }))
    },
      
    postCreate: function() {
      this.inherited(arguments);
      console.log('postCreate');

    },

    startup: function() {
      this.inherited(arguments);
        var projections = this.config.projections
        for (var i = 0; i<projections.length; i++){
            var projectionOption = document.createElement('option')
            projectionOption.value = projections[i].wkid
            projectionOption.innerHTML = projections[i].label
            this.projectionSelect.appendChild(projectionOption)
        }
      console.log('startup');
      
    },

    onOpen: function(){
        console.log('onOpen');
        this.toolTip_Init();
        this.getCoordinate = this.map.on("click", lang.hitch(this, function (e){
            var projectedPoint
            var newDivElement
            var labelText = ""
            this.pointsDropped += 1
            var clickedLocation = e.mapPoint
           /* var clickedGraphic = new graphic(clickedLocation, this.defaultSymbol)
            clickedGraphic.htmlId = 'node_' + this.pointsDropped.toString()
            this.map.graphics.add(clickedGraphic)*/
            var outSpatialReference = this.map.spatialReference
            var geoService = new GeometryService(this.geometryService)
            var params = new ProjectParameters()
            params.geometries = [clickedLocation]
            // if the value in projectionSelect is an int projection based on the WKID else MGRS was chosen
            if(parseInt(this.projectionSelect.value)){ 
                params.outSR = new SpatialReference(parseInt(this.projectionSelect.value)) 
                geoService.project(params,lang.hitch(this, function(result){
                    //var isGeographic = false
                    var projections = this.config.projections
                    //var xCoord
                    //var yCoord
                    var significantDigits
                    for (var i = 0; i<projections.length; i++){

                        if (projections[i].wkid==this.projectionSelect.value){
                            significantDigits = projections[i].significantDigits
                            //console.log(projections[i].isGeographic)
                        }
                    }
                    projectedPoint = result[0]
                    newDivElement = this.createDivElement(clickedLocation)
                    var newDivContent = "Projection: " + this.projectionSelect[this.projectionSelect.selectedIndex].innerHTML + "<br>" + this.nodeTemplate.replace("{pointX}", projectedPoint.x.toFixed(significantDigits).toString() )
                    newDivContent = newDivContent.replace("{pointY}", projectedPoint.y.toFixed(significantDigits).toString() )
                    newDivElement.innerHTML = newDivContent
                    this.resultTextArea.appendChild(newDivElement) 
                    labelText = projectedPoint.x.toFixed(significantDigits).toString() + ", " + projectedPoint.y.toFixed(significantDigits).toString()
                    this.addPointToMapAsGraphic(clickedLocation, labelText)
                    
                }))
            } else {
                outSpatialReference = new SpatialReference(4326) 
                params.outSR = outSpatialReference
                geoService.project(params, lang.hitch(this, function(result){
                    projectedPoint = [result[0].x, result[0].y]
                    //console.log(projectedPoint)
                    var mgrsParams = new ProjectParameters()
                    mgrsParams.sr = outSpatialReference
                    mgrsParams.coordinates = [projectedPoint]
                    mgrsParams.conversionType = "MGRS"
                    mgrsParams.conversionMode = "mgrsNewWith180InZone01"
                    geoService.toGeoCoordinateString(mgrsParams,lang.hitch(this, function(result){
                        //console.log(result)
                        newDivElement = this.createDivElement(clickedLocation)
                        newDivElement.innerHTML = "MGRS: " + result
                        this.resultTextArea.appendChild(newDivElement)
                        labelText = result[0]
                        this.addPointToMapAsGraphic(clickedLocation, labelText)
                                                }))
                }))

            }
    }))
        
    },

      
    addPointToMapAsGraphic: function(pointGeometry, coordinateText){
        var font = new Font("15px", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "Arial");
        /*if(!this.labelLayer){
            this.labelLayer = new GraphicsLayer({id: "coordinateLabelLayer"})
        }
        if(!this.coordinateLayer){
            this.coordinateLayer = new GraphicsLayer({id: "coordinateResultsLayer"})
        }*/
        //this.labelLayer.setMinScale(10000)

            var symbol = this.defaultSymbol

         
            //add the current graphic to the map
/*            var infoTemplate = new InfoTemplate()
            infoTemplate.setTitle("Search Result")            
            infoTemplate.setContent(pointGeometry[i].title) 
            resultGraphic.setInfoTemplate(infoTemplate)*/
            var resultGraphic = new graphic(pointGeometry, symbol)
            resultGraphic.htmlId = 'node_' + this.pointsDropped.toString()
            this.coordinateLayer.add(resultGraphic);
        
            var textSymbol = new TextSymbol(coordinateText,font, new Color([0, 0, 0]));
            textSymbol.setHaloSize(1)
        textSymbol.setVerticalAlignment("top")
        textSymbol.setHaloColor(new Color('#eee'))
        var pnt = new graphic(new Point(pointGeometry), textSymbol)
        pnt.htmlId = 'node_' + this.pointsDropped.toString()
        this.labelLayer.add(pnt)
        this.map.addLayer(this.coordinateLayer)
        this.map.addLayer(this.labelLayer)
        
    },

    onClose: function(){
        this.getCoordinate.remove()
        this.clearGraphics()
        this.mapDiv.removeChild(this.toolTip)
      console.log('onClose');
    },
    
    //Create Div element for coordinate pane  
    createDivElement: function(clickedLocation){
        var newDivElement = document.createElement("DIV")
        //<div id='node_{pointCount}' data-dojo-attach-event='onclick:selectItem' class='coordinateResultNode'>
        newDivElement.className = 'coordinateResultNode'
        newDivElement.id = 'node_' + this.pointsDropped.toString()
        on(newDivElement, "click", lang.hitch(this, function(){
            this.toggleSelect(newDivElement)
                         }))
        newDivElement.data = {}
        newDivElement.data.geometry = clickedLocation
        return newDivElement
    },
    
    clearGraphics: function(){
        //this.map.graphics.clear();
        this.coordinateLayer.clear();
        this.labelLayer.clear();
        this.resultTextArea.innerHTML = ""
        this.itemSelected = false;
    },
      
    toggleSelect: function(inElement){
        var elementList = query(".coordinateResultNode")
        //console.log(elementList)
        if(inElement.classList.contains("selected")){
            inElement.className = "coordinateResultNode"
            this.coordinateLayer.remove(this.coordinateLayer.graphics[this.coordinateLayer.graphics.length-1])
            this.itemSelected = false;
            return
        }
        for(var i = 0; i < elementList.length; i++){
            var classListArray = elementList[i].classList
            if(classListArray.contains("selected")){
                elementList[i].className = "coordinateResultNode"
            }
        }
        if (this.itemSelected){
            for(var i = 0; i<this.coordinateLayer.graphics.length; i++){
                if(this.coordinateLayer.graphics[i].bufferGraphic){
                    this.coordinateLayer.remove(this.coordinateLayer.graphics[i])
                    i--
                }
            }
        }
        //Set buffer symbol properties for when a value is clicked
        this.itemSelected = true;
        var bufferSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 100, null, this.symbolColor50);
        var bufferGraphic = new graphic(inElement.data.geometry, bufferSymbol)
        bufferGraphic.bufferGraphic = true
        this.coordinateLayer.add(bufferGraphic);
        var pinGraphic = new graphic(inElement.data.geometry, this.defaultSymbol)
        pinGraphic.bufferGraphic = true
        this.coordinateLayer.add(pinGraphic);
        
        var newMapExtent
        var geometry = inElement.data.geometry;
        var zoomBuffer = 750
        newMapExtent = new Extent(geometry.x-zoomBuffer, geometry.y-zoomBuffer,geometry.x+zoomBuffer,  geometry.y+zoomBuffer,  this.map.spatialReference)
        this.map.setExtent(newMapExtent); // move to the feature
        inElement.className = "coordinateResultNode selected"

    },
      
    toggleLabel: function(e){
        if(!e.srcElement.checked){
            this.labelLayer.setVisibility(false)
            this.toggleText.innerHTML = "Show"
        } else {this.labelLayer.setVisibility(true);
               this.toggleText.innerHTML = "Hide"}
    },

    removeSelectedGraphic: function(){
        var selectedId = query(".selected")[0].id
        for(var i = 0; i<this.coordinateLayer.graphics.length; i++){
            //console.log(this.map.graphics.graphics[i].htmlId)
            if(this.coordinateLayer.graphics[i].bufferGraphic){
                    this.coordinateLayer.remove(this.coordinateLayer.graphics[i])
                i--
            }
            if(i>-1){
                if(this.coordinateLayer.graphics[i].htmlId == selectedId){
                    this.coordinateLayer.remove(this.coordinateLayer.graphics[i])
                    i--
                }
            }
        }
        for(var i = 0; i<this.labelLayer.graphics.length; i++){
            if(this.labelLayer.graphics[i].htmlId == selectedId){
                this.labelLayer.remove(this.labelLayer.graphics[i])
                i--
            }

        }
        this.resultTextArea.removeChild(query(".selected")[0])
    },

    onMinimize: function(){
      console.log('onMinimize');
    },

    onMaximize: function(){
      console.log('onMaximize');
    },

    onSignIn: function(credential){
      /* jshint unused:false*/
      console.log('onSignIn');
    },

    onSignOut: function(){
      console.log('onSignOut');
    },

    showVertexCount: function(count){
      this.vertexCount.innerHTML = 'The vertex count is: ' + count;
    }
  });
});