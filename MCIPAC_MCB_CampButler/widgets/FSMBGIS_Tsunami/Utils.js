/*
Util for Layer Legend
*/

define([
  'dojo/_base/lang',
  'dojo/_base/array',
  'jimu/LayerInfos/LayerInfos'
], function(lang, array, LayerInfos) {
  
  var mo = {};
  
  mo.getTsunamiLayerInfo = function(config) {

    var layerInfosParam     = [];
    var jimuLayerInfos      = LayerInfos.getInstanceSync();
    var jimuLayerInfoArray  = jimuLayerInfos.getLayerInfoArray();

    for (var i = 0 ; i < jimuLayerInfoArray.length ; i++) {
      var layerInfo = jimuLayerInfoArray[i];
      if (layerInfo.layerObject &&
          (layerInfo.layerObject.declaredClass === 'esri.layers.ArcGISDynamicMapServiceLayer'
           || layerInfo.layerObject.declaredClass === 'esri.layers.ArcGISTiledMapServiceLayer')) {

            var url = layerInfo.layerObject.url;
            if (url.indexOf(config.tsunamiLayerServiceName) > 0) {
              var layerInfoParam = {
                layer: layerInfo.layerObject,
                title: layerInfo.title
              };
              layerInfosParam.push(layerInfoParam);
              break;
            }
      }
    }//for
    return layerInfosParam;
  }//getLayerInfosMap

    
    return mo;
  });
  