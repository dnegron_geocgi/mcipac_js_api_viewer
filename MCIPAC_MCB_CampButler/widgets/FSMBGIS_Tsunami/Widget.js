define([
  'dojo/_base/declare', 'dojo/_base/lang', 'dojo/_base/html', 
  'dojo/_base/array',
  'dojo/on', 
  './Utils',
  'esri/dijit/Legend',
  'dijit/_WidgetsInTemplateMixin',
  'jimu/BaseWidget'
], function(
  declare, lang, html, 
  array,
  on,
  legendUtils,
  Legend,
  _WidgetsInTemplateMixin,
  BaseWidget
  /*CAUTION: do NOT put 'comma' at the last of the object. IE will detect this as an error...*/
) {
  return declare([BaseWidget, _WidgetsInTemplateMixin], {
    baseClass: 'jimu-widget-tsunami',
    name: 'Tsunami Flood Zone',

    _legend: null,


    startup: function() {
      this.inherited(arguments);
    },

    onOpen: function(){
      // 1) Turn on the layer
      this.turnOnTsunamiLayer(true);

      // 2) get the LayerInfo
      var layerInfos = legendUtils.getTsunamiLayerInfo(this.config);
      
      //this._layerInfos = LayerInfos.getInstanceSync();
      var legendParams = {
        arrangement: false,
        autoUpdate: false,
        respectCurrentMapScale: false,
        map: this.map,
        layerInfos: layerInfos
      };
      this._legend = new Legend(legendParams, html.create('div', {}, this.domNode));
      this._legend.startup();
      //this._bindEvent();
    },

    onClose: function() {
      this._legend.destroy();
      this.turnOnTsunamiLayer(false);
    },


    turnOnTsunamiLayer : function(turnOn) {
      //console.log(this.map.layerIds);

      array.forEach(this.map.layerIds, lang.hitch(this, function(layerId){
        if (layerId.indexOf(this.config.tsunamiLayerServiceName) >= 0) {
          // turn on / off
          this.map.getLayer(layerId).setVisibility(turnOn);
          this.map.getLayer(layerId).visibility = turnOn;
        }
      }));
    }



    // onClose: function(){
    //   console.log('onClose');
    // },

    // onMinimize: function(){
    //   console.log('onMinimize');
    // },

    // onMaximize: function(){
    //   console.log('onMaximize');
    // },

    // onSignIn: function(credential){
    //   /* jshint unused:false*/
    //   console.log('onSignIn');
    // },

    // onSignOut: function(){
    //   console.log('onSignOut');
    // }

    // onPositionChange: function(){
    //   console.log('onPositionChange');
    // },

    // resize: function(){
    //   console.log('resize');
    // }

    //methods to communication between widgets:

  });
});