define([
  'dojo/_base/declare', 'dojo/_base/lang', 'dojo/on',
  'esri/tasks/query', 'esri/tasks/QueryTask',
  'dijit/_WidgetsInTemplateMixin',
  'jimu/BaseWidget'
], function(
  declare, lang, on,
  Query, QueryTask,
  _WidgetsInTemplateMixin,
  BaseWidget
  /*CAUTION: do NOT put 'comma' at the last of the object. IE will detect this as an error...*/
) {
  return declare([BaseWidget, _WidgetsInTemplateMixin], {
    baseClass: 'jimu-widget-dms',
    name: 'DMS',

    _queryTasks: null,
    _clickEventHandler: null,
    toolTip_Init: function(){
        this.mapDiv = document.getElementById("map")
        this.toolTip = document.createElement('SPAN')
        this.toolTip.classList.add("dijitTooltipContainer")
        this.toolTip.style.position ="fixed"
        this.toolTip.innerHTML = "Click a facility"
        //this.clickMapTip = 
        //var mapDiv = document.getElementById("map")
        this.mapDiv.appendChild(this.toolTip)
        this.map.on("mouse-move", lang.hitch(this, function(evt) {
            var px, py;
            //console.log(evt)
            if (evt.clientX || evt.pageY) {
              px = evt.clientX;
              py = evt.clientY;
            } else {
                this.toolTip.style.display = "none";
              //px = evt.clientX + dojo.body().scrollLeft - dojo.body().clientLeft;
              //py = evt.clientY + dojo.body().scrollTop - dojo.body().clientTop;
            }
                           
            // dojo.style(tooltip, "display", "none");
            
            this.toolTip.style.display = "none";
            this.toolTip.style.left = (px + 15) + "px"
            this.toolTip.style.top = (py + 15) + "px" ;
            // dojo.style(tooltip, "display", "");
            this.toolTip.style.display = "";
            //console.log(this.toolTip)
            
            // console.log("updated tooltip pos.");
          }));
        this.map.on("mouse-out",lang.hitch(this, function() {
            this.toolTip.style.display = "none";
        }))
    },

    startup: function() {
      this.inherited(arguments);
      this._queryTasks = new Array();
      // init query task:
      for (var i = 0 ; i < this.config.targetLayers.length ; i++) {
        var queryTask = new QueryTask(this.config.targetLayers[i]);
        this._queryTasks.push(queryTask);
      }
    },

    onOpen: function(){
      // *** Prepare event handler to remove when closing the widget (THIS IS TRICKY!) ***
    this.toolTip_Init();
      this.own(this._clickEventHandler = on(this.map, 'click', lang.hitch(this, this.onMapClick))); 
    },

    onClose: function() {
      // Remove 'click' event:
        this.mapDiv.removeChild(this.toolTip)
      if (this._clickEventHandler) {
        this._clickEventHandler.remove();
          
        
        console.log('event removed');
      }
    },

    onMapClick: function(ev) {
      var query = new Query();
      query.spatialRelationship = Query.SPATIAL_REL_WITHIN;
      query.outSpatialReference = this.map.spatialReference;
      query.returnGeometry = true;
      query.geometry = ev.mapPoint;
      query.outFields = ['installationCode', 'facilityNumber'];
      // Sample URL:
      //https://gis.nmci.usmc.mil/dms/search/getList.mil?installation=FOS&facilityNumber=363"
      for (var i = 0 ; i < this._queryTasks.length ; i++) {
        this._queryTasks[i].execute(query, lang.hitch(this, this._onQueryResult), lang.hitch(this,  this._onQueryError));
      }
    },


    _onQueryResult: function(results) {
      if (results.features.length == 0) {
        return;
      }
      var feature = results.features[0];
      var instlnCode = feature.attributes.installationCode;
      var facilNum = feature.attributes.facilityNumber;
      var campCode = this.config.installationCodeMap[instlnCode];
      var selectedDocumentType = ""
      if (this.documentTypeSelection != "Select Document Type"){
          selectedDocumentType = "&drawingType=" + this.documentTypeSelection.value
      }
      var url = this.config.dmsURL + "search/getList.mil?installation="+campCode+"&facilityNumber="+facilNum + selectedDocumentType;
      // Open window:
      console.log(url)
      window.open(url, "DMS");
    },
    _onQueryError: function(err) {
      console.log('Query Error: ' + err);
    },



    // onClose: function(){
    //   console.log('onClose');
    // },

    // onMinimize: function(){
    //   console.log('onMinimize');
    // },

    // onMaximize: function(){
    //   console.log('onMaximize');
    // },

    // onSignIn: function(credential){
    //   /* jshint unused:false*/
    //   console.log('onSignIn');
    // },

    // onSignOut: function(){
    //   console.log('onSignOut');
    // }

    // onPositionChange: function(){
    //   console.log('onPositionChange');
    // },

    // resize: function(){
    //   console.log('resize');
    // }

    //methods to communication between widgets:

  });
});